exports.index = function(req, res){
  res.render('index', { title: 'Express' });
};

exports.content_slug = function(req, res){
	var slug = req.param("slug");
	var translateUri = require('../config/translateUri');
	var uri = translateUri.getUriByContent(req.param("content"))

	var Client = require('node-rest-client').Client;
	client = new Client();

	client.get(uri + slug, function(data, response){
		try{
			if(response.statusCode == 200){
				if(typeof(data) == "string"){
					data = JSON.parse(data);
				}

				var jsonLite = require('../config/jsonLite');
	    	res.json(jsonLite.render(data));
			}
			else{
	    	res.json({text:'slug não encontrada'}, 404);
			}
		}
		catch(e){
    	res.json({error:e.message, data:data}, 500);
		}
  });
};

