var request = require('supertest'),
	  express = require('express'),
	  routes = require('../routes');

var app = express();
app.get('/:content/:slug', routes.content_slug);

describe('estabelecimentos', function(){
	describe('GET /estabelecimentos/boi-preto 200', function(){
	  it('respond with json', function(done){
	    request(app)
	      .get('/estabelecimentos/boi-preto')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(200, done);
	  })
	})

	describe('GET /estabelecimentos/boi 404', function(){
	  it('respond with json', function(done){
	    request(app)
	      .get('/estabelecimentos/boi')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(404, done);
	  })
	})
})