var request = require('supertest'),
	  express = require('express'),
	  routes = require('../routes');

var app = express();
app.get('/:content/:slug', routes.content_slug);

describe('atracoes', function(){
	describe('GET /atracoes/caixa-preta 200', function(){
	  it('respond with json', function(done){
	    request(app)
	      .get('/atracoes/caixa-preta')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(200, done);
	  })
	})

	describe('GET /atracoes/caixa 404', function(){
	  it('respond with json', function(done){
	    request(app)
	      .get('/atracoes/caixa')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(404, done);
	  })
	})
})