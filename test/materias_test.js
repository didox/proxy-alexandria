var request = require('supertest'),
	  express = require('express'),
	  routes = require('../routes');

var app = express();
app.get('/:content/:slug', routes.content_slug);

describe('materias', function(){
	describe('GET /materias/leo-teste-video-youtube 200', function(){
	  it('respond with json', function(done){
	    request(app)
	      .get('/materias/leo-teste-video-youtube')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(200, done);
	  })
	})

	describe('GET /materias/nao-existe 404', function(){
	  it('respond with json', function(done){
	    request(app)
	      .get('/materias/nao-existe')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(404, done);
	  })
	})
})