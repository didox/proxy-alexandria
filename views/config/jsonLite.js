JSON_ESTABELECIMENTO = ["nome", "categorias", "criacao"];

exports.render = function(json){
	if(json.tipo_recurso == "estabelecimento"){
		jEstabelecimento = {};
		for(var index in JSON_ESTABELECIMENTO){
			var key = JSON_ESTABELECIMENTO[index];
			var value = json[key];
			jEstabelecimento[key] = json[key];
		}
		return jEstabelecimento;
	}

	return json;
}