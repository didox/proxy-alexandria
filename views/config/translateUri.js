exports.getUriByContent = function(content){
	if(content == "estabelecimentos"){
		return "http://stage.estabelecimentos.api.abril.com.br/estabelecimentos/"
	}
	else if(content == "materias"){
		return "http://stage.editorial.api.abril.com.br/materias/"
	}
	else if(content == "atracoes"){
		return "http://stage.atracoes.api.abril.com.br/atracoes/"
	}

	return "http://stage.editorial.api.abril.com.br/"
}